package com.company.luongchung.tkbtlu.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.company.luongchung.tkbtlu.R;
import com.company.luongchung.tkbtlu.model.TinNhan;
import com.facebook.login.widget.ProfilePictureView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class adapterTinNhan extends ArrayAdapter<TinNhan> {

    Activity context;
    int resource;
    ArrayList<TinNhan> objects;


    public adapterTinNhan(@NonNull Activity context, @LayoutRes int resource, @NonNull ArrayList<TinNhan> objects) {
        super(context, resource, objects);
        this.context=context;
        this.objects=objects;
        this.resource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = this.context.getLayoutInflater();
        Typeface font = Typeface.createFromAsset(this.context.getAssets(),"fonts/luongchung.ttf");
        View row = layoutInflater.inflate(this.resource, null);
        TextView name= (TextView) row.findViewById(R.id.id_nameUser);
        TextView tinnhan= (TextView) row.findViewById(R.id.id_tinNhanUser);
        ProfilePictureView anh= row.findViewById(R.id.id_avatarUser);

        final TinNhan tinNhan =objects.get(position);

        name.setText(tinNhan.getName()+": ");
        if(tinNhan.getIsAdmin())name.setTextColor(context.getResources().getColor(R.color.Do));
        tinnhan.setText(tinNhan.getNoiDung());
        name.setTypeface(font);
        tinnhan.setTypeface(font);
        anh.setProfileId(tinNhan.getId());
        return row;
    }
}
