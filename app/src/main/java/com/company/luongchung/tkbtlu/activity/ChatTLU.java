package com.company.luongchung.tkbtlu.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.company.luongchung.tkbtlu.R;
import com.company.luongchung.tkbtlu.adapter.adapterTinNhan;
import com.company.luongchung.tkbtlu.model.TinNhan;
import com.facebook.Profile;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChatTLU extends AppCompatActivity {
    int CODE_REMOVE=30;
    String nameTable="ChatTLU";
    DatabaseReference mDatabase;
    ArrayList<TinNhan> arrTinNhan;
    adapterTinNhan adapterTinNhan;
    ListView lvChat;
    Button btnGui;
    EditText txtND;
    ArrayList<String> arrKhoa=new ArrayList<>();
    ArrayList<String> arrID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_tlu);
        arrID =new ArrayList<>();
        addControlls();
        addFireBase();
        addEvents();
    }
    private void addFireBase() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref1= FirebaseDatabase.getInstance().getReference();
        final DatabaseReference ref2,ref3;
        ref2 = ref1.child(nameTable);
        ref3 = ref1.child("Khoa");
        ref3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                    arrKhoa.add(String.valueOf(dsp.getValue()));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ref2.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                TinNhan job = dataSnapshot.getValue(TinNhan.class);
                arrTinNhan.add(job);
                adapterTinNhan.notifyDataSetChanged();
                arrID.add(dataSnapshot.getKey());
                if(arrID.size()>=CODE_REMOVE){
                    RemoveFull(ref2);
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void RemoveFull(DatabaseReference ref2) {
        for (int i=0;i<arrID.size()-CODE_REMOVE;i++)
            ref2.child(arrID.get(i)).removeValue();
    }
    private void addTinNhan(String id, String name, String noiDung, boolean isAdmin) {
        TinNhan user = new TinNhan(id,name,noiDung,isAdmin);
        mDatabase.child(nameTable).push().setValue(user);
    }
    private void addEvents() {
        btnGui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtND.getText().toString().length()<3){
                    Toast.makeText(ChatTLU.this, "Nội dung quá ngắn !", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(kiemtra()){
                    Toast.makeText(ChatTLU.this,
                                      "Tài khoản bạn đã bị khóa tính năng chat :)" +
                                        "\n ---------Thân ái chào tạm biệt !----------", Toast.LENGTH_SHORT).show();
                    return;
                }
                addTinNhan(Profile.getCurrentProfile().getId(),
                        Profile.getCurrentProfile().getName(),
                        txtND.getText().toString(),false);
                txtND.setText("");
            }
        });
    }
    private boolean kiemtra() {
        String tmp="777";
        try {
            tmp=Profile.getCurrentProfile().getId();
        }catch (Exception ex){

        }

        for (String i:arrKhoa){
            if(tmp.equals(i))return true;
        }
        return false;
    }
    private void addControlls() {
        lvChat =findViewById(R.id.lv_chat);
        btnGui=findViewById(R.id.btnGui);
        txtND=findViewById(R.id.txtND);
        arrTinNhan=new ArrayList<>();
        adapterTinNhan=new adapterTinNhan(ChatTLU.this,R.layout.item_chat,arrTinNhan);
        lvChat.setAdapter(adapterTinNhan);
    }
}
