package com.company.luongchung.tkbtlu.service;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;


import com.company.luongchung.tkbtlu.R;
import com.company.luongchung.tkbtlu.activity.MainActivity;
import com.company.luongchung.tkbtlu.activity.Page1;
import com.company.luongchung.tkbtlu.model.lich_chuan;
import com.company.luongchung.tkbtlu.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


/**
 * Created by DUC THANG on 9/5/2017.
 */

public class ServiceNoti extends Service {
    private static final String TAG = "Service";
    String luuCheckThongBao="luucheckthongbaos";
    NotificationManager notificationManager;
    Timer timer;
    private ArrayList<lich_chuan> listHomNay;
    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    SimpleDateFormat sf1 = new SimpleDateFormat("dd/MM/yyyy");
    int level_battery = 0;
    private Handler handler;

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        handler = new Handler() {
            @Override
            public void publish(LogRecord record) {}
            @Override
            public void flush() {}
            @Override
            public void close() throws SecurityException {}
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    Date currentTime = Calendar.getInstance().getTime();
                    listHomNay = Page1.getLichHomNay();
                    if(listHomNay != null)
                    {
                        SharedPreferences sharedPreferences= getSharedPreferences(luuCheckThongBao,MODE_PRIVATE);
                        Boolean kt=sharedPreferences.getBoolean("thongbaos",false);
                        if(!kt) return;
                        for(int i = 0; i < listHomNay.size(); i++)
                        {
                            String thoigianhoc = Utils.instance.ThoiGianHoc(Integer.parseInt(listHomNay.get(i).getTietBatDau()));
                            Log.e(TAG, listHomNay.get(i).toString());
                            Date lichhoc;
                            lichhoc = sf.parse(sf1.format(Calendar.getInstance().getTime()) + thoigianhoc);
                            Log.e(TAG, lichhoc.toString());
                            long diff = lichhoc.getTime() - currentTime.getTime();
                            long diffMinutes = diff / (60 * 1000);
                            Log.e(TAG, diffMinutes + " - " + diff);
                            if(diffMinutes > 0 && diffMinutes <= 5)
                            {
                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.drawable.ic_activity_active)
                                                .setContentTitle("Lịch học sắp tới "+ diffMinutes +" phút nữa!")
                                                .setContentText(listHomNay.get(i).getTenMonHoc() + " - " + listHomNay.get(i).getDiaDiem() + " - " + thoigianhoc)
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                                mBuilder.build();
                                Intent nIntent = getPreviousIntent();
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
                                stackBuilder.addParentStack(MainActivity.class);
                                stackBuilder.addNextIntent(nIntent);
                                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                                mBuilder.setContentIntent(pendingIntent);
                                startForeground(1, mBuilder.build());
                                notificationManager.notify(1, mBuilder.build());
                            }
                            else if(diffMinutes <= 0 && diffMinutes >= -1)
                            {
                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.drawable.ic_feed)
                                                .setContentTitle("Chúc bạn học tốt!")
                                                .setContentText("Đã đến giờ học " + listHomNay.get(i).getTenMonHoc())
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                                mBuilder.build();
                                Intent nIntent = getPreviousIntent();
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
                                // Adds the back stack
                                stackBuilder.addParentStack(MainActivity.class);
                                stackBuilder.addNextIntent(nIntent);
                                PendingIntent pendingIntent =
                                        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                                mBuilder.setContentIntent(pendingIntent);

                                startForeground(1, mBuilder.build());

                                notificationManager.notify(1, mBuilder.build());
                            }
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.e(TAG, e.toString());
                }

            }
        }, 0, 60000);
    }

    private Intent getPreviousIntent() {
        Intent newIntent = null;
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final List<ActivityManager.AppTask> recentTaskInfos = activityManager.getAppTasks();
            if (!recentTaskInfos.isEmpty()) {
                for (ActivityManager.AppTask appTaskTaskInfo: recentTaskInfos) {
                    if (appTaskTaskInfo.getTaskInfo().baseIntent.getComponent().getPackageName().equals("com.company.luongchung.tlulichhoc")) {
                        newIntent = appTaskTaskInfo.getTaskInfo().baseIntent;
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                }
            }
        } else {
            final List<ActivityManager.RecentTaskInfo> recentTaskInfos = activityManager.getRecentTasks(1024, 0);
            if (!recentTaskInfos.isEmpty()) {
                for (ActivityManager.RecentTaskInfo recentTaskInfo: recentTaskInfos) {
                    if (recentTaskInfo.baseIntent.getComponent().getPackageName().equals("com.company.luongchung.tlulichhoc")) {
                        newIntent = recentTaskInfo.baseIntent;
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                }
            }
        }
        if (newIntent == null) newIntent = new Intent();
        return newIntent;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public int onStartCommand(Intent intent, final int flags, int startId) {
        return START_STICKY;
    }

    private final IBinder mBinder = new KillBinder(this);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class KillBinder extends Binder {
        public final Service service;

        public KillBinder(Service service) {
            this.service = service;
        }

    }
}