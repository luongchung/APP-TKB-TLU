package com.company.luongchung.tkbtlu.activity;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.company.luongchung.tkbtlu.Interface.Icamera;
import com.company.luongchung.tkbtlu.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;


public class chitiet_lichhoc extends AppCompatActivity implements Icamera {
    private TextView txtMain;
    private TextView TenMon,TenLopTC,GiangVien,DiaDiem,TietHoc,ThoiGian,SoTC;
    Intent intent;
    private AdView mAdView1,mAdView2,mAdView3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chitiet_lichhoc);
        addControls();
        quangcao();
    }
    public void quangcao(){
        MobileAds.initialize(chitiet_lichhoc.this, "ca-app-pub-5001443737686857~4542932552");
        mAdView1 = (AdView) findViewById(R.id.adView1);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        mAdView1.loadAd(adRequest1);


        mAdView2 = (AdView) findViewById(R.id.adView2);
        AdRequest adRequest2 = new AdRequest.Builder().build();
        mAdView2.loadAd(adRequest2);


        mAdView3 = (AdView) findViewById(R.id.adView3);
        AdRequest adRequest3 = new AdRequest.Builder().build();
        mAdView3.loadAd(adRequest3);
    }
    private void addControls() {
        Typeface font = Typeface.createFromAsset(this.getAssets(),"fonts/luongchung.ttf");
        txtMain= (TextView) findViewById(R.id.txt_main);
        txtMain.setTypeface(font);
        TenMon= (TextView) findViewById(R.id.id_tenmon);
        TenLopTC= (TextView) findViewById(R.id.id_tenloptinchi);
        GiangVien=(TextView)findViewById(R.id.id_giangvien);
        DiaDiem=(TextView)findViewById(R.id.id_diadiem);
        ThoiGian=(TextView)findViewById(R.id.id_thoigianhoc);
        SoTC=(TextView)findViewById(R.id.id_sotinchi);
        TietHoc=(TextView)findViewById(R.id.id_tiethoc);
        intent =getIntent();
        TenMon.setText(intent.getStringExtra("TenMonHoc"));
        TenLopTC.setText("Tên lớp tín chỉ: "+intent.getStringExtra("TenLopTC"));
        DiaDiem.setText("Địa điểm: "+intent.getStringExtra("DiaDiem"));
        GiangVien.setText("Giảng viên: "+intent.getStringExtra("TenGV"));
        SoTC.setText("Số tín chỉ: "+intent.getStringExtra("SoTC"));
        TietHoc.setText("Tiết học: "+intent.getStringExtra("TietBD")+"-->"+intent.getStringExtra("TietKT"));
        ThoiGian.setText("Ngày học: "+intent.getStringExtra("NgayHoc"));
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void xu_ly_click_camera(int i) {
    }
}

