package com.company.luongchung.tkbtlu.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.company.luongchung.tkbtlu.R;

import static android.content.Context.MODE_PRIVATE;

public class ViewDialogSuaLink {
    public void showDialog(final Activity activity){
        final Dialog dialog = new Dialog(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popupupdatelink);
        dialog.show();
        Button thoat = (Button) dialog.findViewById(R.id.btnThoat);
        Button luu = (Button) dialog.findViewById(R.id.btnLuuURL);
        SharedPreferences sharedPreferences= activity.getSharedPreferences(activity.getString(R.string.luuURL),MODE_PRIVATE);
        String url=sharedPreferences.getString("URL",activity.getString(R.string.linkdangky));
        final TextView txtUrl=dialog.findViewById(R.id.txturldangky);
        txtUrl.setText(url);
        thoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        luu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //lưu url
                try {
                SharedPreferences sharedPreferences= activity.getSharedPreferences(activity.getString(R.string.luuURL),MODE_PRIVATE);
                //ghivao
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("URL",txtUrl.getText().toString());
                editor.commit();
                    Toast.makeText(activity, "Lưu thành công !", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Toast.makeText(activity, "Lưu không thành công!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
