package com.company.luongchung.tkbtlu.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import com.company.luongchung.tkbtlu.R;

public class Setting extends AppCompatActivity {
    String luuCheckThongBao="luucheckthongbaos";
    Switch aSwitch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        addControls();
        addEvents();
        loadSetting();
    }
    private void loadSetting() {
        SharedPreferences sharedPreferences= getSharedPreferences(luuCheckThongBao,MODE_PRIVATE);
        Boolean kt=sharedPreferences.getBoolean("thongbaos",false);
        if(kt)aSwitch.setChecked(true);
        else aSwitch.setChecked(false);
    }
    private void addEvents() {
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xulyClick();
            }
        });
    }
    private void xulyClick() {
        if(aSwitch.isChecked())
        {
            SharedPreferences sharedPreferences= getSharedPreferences(luuCheckThongBao,MODE_PRIVATE);
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putBoolean("thongbaos",true);
            editor.commit();
            Toast.makeText(Setting.this,"Đã bật", Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPreferences sharedPreferences= getSharedPreferences(luuCheckThongBao,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("thongbaos",false);
        editor.commit();
        Toast.makeText(Setting.this,"Đã tắt", Toast.LENGTH_SHORT).show();
    }
    private void addControls() {
        aSwitch= (Switch) findViewById(R.id.switch1);
    }
}
