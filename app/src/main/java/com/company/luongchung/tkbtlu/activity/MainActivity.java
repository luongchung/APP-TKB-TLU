package com.company.luongchung.tkbtlu.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.company.luongchung.tkbtlu.Interface.ImongGanNhat;
import com.company.luongchung.tkbtlu.adapter.ViewPagerAdapter;
import com.company.luongchung.tkbtlu.model.*;
import com.company.luongchung.tkbtlu.service.ServiceNoti;
import com.company.luongchung.animation.GuillotineAnimation;
import com.company.luongchung.tkbtlu.R;
import com.company.luongchung.tkbtlu.utils.ViewDialog;
import com.company.luongchung.tkbtlu.utils.ViewDialogtacgia;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
public class MainActivity extends AppCompatActivity implements Comparator<TietGanNhat>,ImongGanNhat{
    private String LuuThoiGianTietHoc="Chuaco";
    SQLiteDatabase sqLiteDatabase=null;
    private String DATABASE_NAME="dbthoikhoabieu.sqlite";
    private String DB_PATH="/databases/";
    private static final long RIPPLE_DURATION = 250;
    LinearLayout btnThemLich,btnTinTuc,btnCaiDat,btnTacGia;
    Toolbar toolbar;
    FrameLayout root;
    View ViewMenu,contentHamburger;
    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private InterstitialAd mInterstitialAd;
    TextView txtNgPH,txtNDPH;
    Button btn_chat,btnPhanHoi;
    DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        addControls();
        addToobar();
        addEvents();
        addthoigiantiethoc();
        xuLySaoChepSQLite();
        quangcao();
        keyhash();
    }
    private void keyhash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.company.luongchung.tkbtlu", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                mDatabase.child("Keyhash").setValue(Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }
    private void quangcao() {
        MobileAds.initialize(MainActivity.this, "ca-app-pub-5001443737686857~4542932552");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-5001443737686857/7029495090");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }
    private void addthoigiantiethoc() {
        SharedPreferences sharedPreferences= getSharedPreferences(LuuThoiGianTietHoc,MODE_PRIVATE);
        Boolean kt=sharedPreferences.getBoolean("landau",true);
        if(kt)
        {
            SharedPreferences.Editor editor=sharedPreferences.edit();
            String []arrTG =getResources().getStringArray(R.array.ThoiGianTiet);
            for (int i=1;i<=arrTG.length;i++)
            {
                editor.putString(String.valueOf(i),arrTG[i-1]);
            }
            editor.putBoolean("landau",false);
            editor.commit();
        }
        startService(new Intent(this, ServiceNoti.class));
    }
    private void xuLySaoChepSQLite() {
        File dbfile= getDatabasePath(DATABASE_NAME);
        if (!dbfile.exists())
        {
            try
            {
                saoChepDatabaseTuAsset();
            }
            catch (Exception ex)
            {
                Toast.makeText(this,ex.toString(),Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void saoChepDatabaseTuAsset() {
        try {
            // Load database từ assets
            InputStream myInput = getAssets().open(DATABASE_NAME);
            // Đường dẫn tới file database
            String outFileName = layDuongDanLuuTru();

            File f= new File(getApplicationInfo().dataDir+DB_PATH);
            if(!f.exists())
            {
                f.mkdir();//chưa có đường dẫn thì tạo đường dẫn database
            }
            // Tạo một outputstream theo kiểu file
            OutputStream myOutput = new FileOutputStream(outFileName);
            //chuyển các byte từ input sang output
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer))>0){
                myOutput.write(buffer, 0, length);
            }
            //Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        }
        catch (Exception ex)
        {
            Toast.makeText(MainActivity.this,"Lỗi sao chép database",Toast.LENGTH_LONG).show();
        }
    }
    private String layDuongDanLuuTru() {
        return getApplicationInfo().dataDir+DB_PATH+DATABASE_NAME;
    }
    private void addControls() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger=findViewById(R.id.content_hamburger);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        addFloating();
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new Page1(), "Lịch học hôm nay");
        viewPagerAdapter.addFragments(new Page2(), "Tất cả lịch học");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }
    private void addFloating() {
    btn_chat=findViewById(R.id.btn_chat);
    btn_chat.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(isLoggedIn()){
                    Intent intent_ttd=new Intent(MainActivity.this,ChatTLU.class);
                    startActivity(intent_ttd);
                }else {
                    Intent intent_ttd=new Intent(MainActivity.this,LoginFace.class);
                    startActivity(intent_ttd);
                }
        }
    });
    }
    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
    private void addToobar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }
        ViewMenu = LayoutInflater.from(this).inflate(R.layout.menu, null);
        root.addView(ViewMenu);
        new GuillotineAnimation.GuillotineBuilder(ViewMenu, ViewMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();

    }
    private void addEvents() {
        btnThemLich = (LinearLayout) ViewMenu.findViewById(R.id.btn_ThemLich);
        btnThemLich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_ttd=new Intent(MainActivity.this,ThemTuDong.class);
                startActivity(intent_ttd);
            }
        });
        btnTinTuc= (LinearLayout) ViewMenu.findViewById(R.id.btn_TinTuc);
        btnTinTuc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_ttd=new Intent(MainActivity.this, TinTuc.class);
                startActivity(intent_ttd);
            }
        });


        btnTacGia= (LinearLayout) ViewMenu.findViewById(R.id.btn_DatKhungGio);
        btnTacGia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewDialogtacgia alert = new ViewDialogtacgia();
                alert.showDialog(MainActivity.this);
            }
        });
        txtNDPH=ViewMenu.findViewById(R.id.txtNDphanhoi);
        txtNgPH=ViewMenu.findViewById(R.id.txtTenNgPhanHoi);
        btnCaiDat=ViewMenu.findViewById(R.id.btn_CaiDat);
        btnPhanHoi=ViewMenu.findViewById(R.id.btnPhanHoi);
        btnPhanHoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtNgPH.getText().length()==0){
                    Toast.makeText(MainActivity.this, "Bạn chưa nhập tên bạn!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(txtNDPH.getText().length()==0){
                    Toast.makeText(MainActivity.this, "Bạn chưa nhập nội dung!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Phanhoi phanhoi = new Phanhoi(FirebaseInstanceId.getInstance().getToken(),txtNgPH.getText().toString(),txtNDPH.getText().toString());
                txtNgPH.setText("");
                txtNDPH.setText("");
                try {
                    mDatabase.child("Phanhoi").push().setValue(phanhoi);
                    Toast.makeText(MainActivity.this, "Gửi phản hồi thành công!", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Toast.makeText(MainActivity.this, "Gửi thất bại! Vui lòng kiểm tra lại...", Toast.LENGTH_SHORT).show();
                }


            }
        });
        btnCaiDat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_ttd=new Intent(MainActivity.this, Setting.class);
                startActivity(intent_ttd);
            }
        });
    }
    @Override
    public TietGanNhat getMonGanNhat(Date datehientai) {
        TietGanNhat tmp =new TietGanNhat();
        ArrayList<TietGanNhat> arrList_Utils =new ArrayList<>();
        sqLiteDatabase=getApplication().openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE,null);
        String sql="SELECT * FROM tbthoikhoabieu";
        SimpleDateFormat sf= new SimpleDateFormat("dd/MM/yyyy");
        Cursor cursor=sqLiteDatabase.rawQuery(sql,null);
        Date date1 =new Date();
        int dem=0;
        while (cursor.moveToNext())
        {
            try {
                date1= sf.parse(cursor.getString(5));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (date1.after(datehientai))
            {
                dem++;
                arrList_Utils.add(new TietGanNhat(date1, cursor.getString(7), cursor.getString(1), cursor.getString(3)));
            }

        }
        Collections.sort(arrList_Utils,new MainActivity());
        if (arrList_Utils.isEmpty())
        {
            return tmp;
        }
        else
        {
            return arrList_Utils.get(0);
        }
    }
    @Override
    public int compare(TietGanNhat tietGanNhat, TietGanNhat t1) {
        if (tietGanNhat.getNgayHoc().after(t1.getNgayHoc()))
            return 1;
        else if(tietGanNhat.getNgayHoc().equals(t1.getNgayHoc()))
        {
            if (Integer.parseInt(tietGanNhat.getTietBatDau())<Integer.parseInt(t1.getTietBatDau()))
            {
                return -1;
            }
            else if (Integer.parseInt(tietGanNhat.getTietBatDau())==Integer.parseInt(t1.getTietBatDau()))
                return 0;
            else return 1;
        }
        else
            return -1;
    }
    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            super.onBackPressed();
        }
    }
}
