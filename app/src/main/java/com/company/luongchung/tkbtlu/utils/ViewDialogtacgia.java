package com.company.luongchung.tkbtlu.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import com.company.luongchung.tkbtlu.R;

public class ViewDialogtacgia {
    public void showDialog(final Activity activity){
        final Dialog dialog = new Dialog(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.tacgia);
        dialog.show();
        Button btnThoat = (Button) dialog.findViewById(R.id.btnThoat);
        ImageButton btnFacebook = (ImageButton) dialog.findViewById(R.id.btnFacebook);
        ImageButton btnGmail = (ImageButton) dialog.findViewById(R.id.btnGmail);
        ImageButton btnGithub = (ImageButton) dialog.findViewById(R.id.btnGithub);
        btnThoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/100004154587803"));
                    activity.startActivity(intent);
                } catch(Exception e) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/L.u.o.n.g.C.h.u.n.g.W.R.U")));
                }
            }
        });
        btnGithub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(Intent.ACTION_VIEW,    Uri.parse("http://github.com/luongchung")));
            }
        });
        btnGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent=new Intent(Intent.ACTION_SEND);
                    String[] recipients={"chunglv42@wru.vn"};
                    intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                    intent.putExtra(Intent.EXTRA_SUBJECT,"ĐÓNG GÓP Ý KIẾN APP TKB THỦY LỢI");
                    intent.putExtra(Intent.EXTRA_TEXT,"Mời điền nội dung...");
                    intent.setType("text/html");
                    intent.setPackage("com.google.android.gm");
                    activity.startActivity(Intent.createChooser(intent, "Send mail"));
                }catch (Exception ex){

                }

            }
        });
    }
}
