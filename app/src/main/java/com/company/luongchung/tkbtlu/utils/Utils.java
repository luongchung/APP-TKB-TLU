package com.company.luongchung.tkbtlu.utils;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.company.luongchung.tkbtlu.activity.MainActivity;
import com.company.luongchung.tkbtlu.model.TietGanNhat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by luongchung on 9/5/17.
 */

public class Utils {
    private static final String TAG = "Utils";
    private Application context;
    private MainActivity mainActivity;
    public static Utils instance = new Utils();
    private Utils()
    {
    }

    public void setContext(Application context) {
        this.context = context;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    SQLiteDatabase sqLiteDatabase=null;
    private String DATABASE_NAME="dbthoikhoabieu.sqlite";
    private String DB_PATH="/databases/";

    public String ThoiGianHoc(int tietbatdau)
    {
        String thoigianhoc = "";
        switch(tietbatdau)
        {
            case 1:
                thoigianhoc = " 06:55:00";
                break;
            case 2:
                thoigianhoc = " 07:50:00";
                break;
            case 3:
                thoigianhoc = " 08:45:00";
                break;
            case 4:
                thoigianhoc = " 09:40:00";
                break;
            case 5:
                thoigianhoc = " 10:35:00";
                break;
            case 6:
                thoigianhoc = " 11:30:00";
                break;
            case 7:
                thoigianhoc = " 12:50:00";
                break;
            case 8:
                thoigianhoc = " 13:45:00";
                break;
            case 9:
                thoigianhoc = " 14:40:00";
                break;
            case 10:
                thoigianhoc = " 15:35:00";
                break;
            case 11:
                thoigianhoc = " 16:30:00";
                break;
            case 12:
                thoigianhoc = " 17:25:00";
                break;
            case 13:
                thoigianhoc = " 18:45:00";
                break;
            case 14:
                thoigianhoc = " 19:40:00";
                break;
            case 15:
                thoigianhoc = " 20:35:00";
                break;
        }

        return thoigianhoc;
    }

    public TietGanNhat hamlay_1Mon_GanNhat(Date NgayHienTai) ////trả về Tiết gần nhất - truyền vào ngày hiện tại dd/MM/yyy
    {
        TietGanNhat tmp =new TietGanNhat();
        ArrayList<TietGanNhat> arrList_Utils =new ArrayList<>();
        sqLiteDatabase= context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE,null);
        Log.e(TAG, sqLiteDatabase.toString() + " context: " + context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE,null));
        String sql="SELECT * FROM tbthoikhoabieu";
        SimpleDateFormat sf= new SimpleDateFormat("dd/MM/yyyy");
        Cursor cursor=sqLiteDatabase.rawQuery(sql,null);
        Date date1 =new Date();
        while (cursor.moveToNext())
        {
            try {
                date1= sf.parse(cursor.getString(5));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (date1.before(NgayHienTai))
            {
                arrList_Utils.add(new TietGanNhat(date1, cursor.getString(7), cursor.getString(1), cursor.getString(3)));
            }

        }
        Collections.sort(arrList_Utils,new MainActivity());
        if (arrList_Utils.isEmpty())
        {
            return tmp;
        }
        else
        {
            return arrList_Utils.get(0);
        }
    }
}